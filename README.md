LaTeX Beamer Theme following the MPI 2015 corporate design rules for
presentation.

See the example directory for a sample beamer presentation explaining
the usage.

Make sure you put the folder in a path that is recursively searched by latex. 
This can e.g. be your texmf folder in the homedirectory on a recent Ubuntu 
system. Otherwise the logos will not be found.

For further information see
http://wiki.mpi-magdeburg.mpg.de/mpi/index.php/MPI2015_LaTeX_Presentation_Template
